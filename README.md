# README #

See the testing suit `TorSOCKS_test.go` for examples.

An extremely simple library for using go.net's SOCKS5 proxy for Dialing through Tor. See `code.google.com/p/go.net/proxy`.

Public domain
